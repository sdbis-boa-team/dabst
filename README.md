# Database Smart Web Tool
##### You can read the docs here: https://sdbis-boa-team.gitlab.io/dabst
##### ~~Application live at: https://dabst.oiste.ro~~ Demo video here: https://sdbis-boa-team.gitlab.io/dabst/demo.mp4
##### ~~OpenAPI Swagger Documentation: https://dabst.oiste.ro/swagger-ui/index.html~~

##### Deployment
* make sure you have Docker and docker-compose installed
* make sure you have the database running & data imported
* copy `.env.` from `.env.dist` (and put in actual values)
* Run `make api client`
