package com.sdbis.dabst.controller;

import java.util.List;

import com.sdbis.dabst.exception.ResourceNotFoundException;
import com.sdbis.dabst.repository.DatabaseRepositoryImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.sdbis.dabst.model.Database;
import com.sdbis.dabst.repository.DatabaseRepository;

@RestController
@RequestMapping("/api/v1")
@CrossOrigin(origins = "*")
public class DatabaseController {

    private final DatabaseRepository databaseRepository;
    private final DatabaseRepositoryImpl databaseRepositoryImpl;

    @Autowired
    public DatabaseController(DatabaseRepository databaseRepository, DatabaseRepositoryImpl databaseRepositoryImpl) {
        this.databaseRepository = databaseRepository;
        this.databaseRepositoryImpl = databaseRepositoryImpl;
    }

    @GetMapping("/databases/all")
    public List<Database> getAllDatabases() {
        return databaseRepository.findAll();
    }

    @GetMapping("/databases/{id}")
    public ResponseEntity<Database> getById(@PathVariable(value = "id") Long id) throws ResourceNotFoundException {
        Database database = databaseRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Database not found for this id: " + id));
        return ResponseEntity.ok().body(database);
    }

    @GetMapping("/databases/most-popular")
    public List<Database> getMostPopularDatabases(@RequestParam Integer limit) {
        return databaseRepositoryImpl.findOrderedByViewCountLimitedTo(limit);
    }

    @GetMapping("/databases")
    public List<Database> getDatabasesByCriteria(
            @RequestParam(required = false) String name,
            @RequestParam(required = false) String langWrittenIn,
            @RequestParam(required = false) String langSupported,
            @RequestParam(required = false) String license,
            @RequestParam(required = false) String os,
            @RequestParam(required = false) String type
    ) {
        return databaseRepositoryImpl.searchByCriteria(name, langWrittenIn, langSupported, license, os, type);
    }
}
