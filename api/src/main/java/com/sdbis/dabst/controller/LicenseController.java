package com.sdbis.dabst.controller;

import com.sdbis.dabst.model.License;
import com.sdbis.dabst.repository.LicenseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/v1")
@CrossOrigin(origins = "*")
public class LicenseController {

    private final LicenseRepository licenseRepository;

    @Autowired
    public LicenseController(LicenseRepository licenseRepository) {
        this.licenseRepository = licenseRepository;
    }

    @GetMapping("/licenses/all")
    public List<License> getAllProgrammingLanguages() {
        return licenseRepository.findAll();
    }
}
