package com.sdbis.dabst.controller;

import com.sdbis.dabst.model.OperatingSystem;
import com.sdbis.dabst.repository.OperatingSystemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/v1")
@CrossOrigin(origins = "*")
public class OperatingSystemController {

    private final OperatingSystemRepository operatingSystemRepository;

    @Autowired
    public OperatingSystemController(OperatingSystemRepository operatingSystemRepository) {
        this.operatingSystemRepository = operatingSystemRepository;
    }

    @GetMapping("/operating-systems/all")
    public List<OperatingSystem> getAllProgrammingLanguages() {
        return operatingSystemRepository.findAll();
    }
}
