package com.sdbis.dabst.controller;

import com.sdbis.dabst.model.ProjectType;
import com.sdbis.dabst.repository.ProjectTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/v1")
@CrossOrigin(origins = "*")
public class ProjectTypeController {

    private final ProjectTypeRepository projectTypeRepository;

    @Autowired
    public ProjectTypeController(ProjectTypeRepository projectTypeRepository) {
        this.projectTypeRepository = projectTypeRepository;
    }

    @GetMapping("/project-types/all")
    public List<ProjectType> getAllProgrammingLanguages() {
        return projectTypeRepository.findAll();
    }
}
