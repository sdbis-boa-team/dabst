package com.sdbis.dabst;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DabstApplication {

    public static void main(String[] args) {
        SpringApplication.run(DabstApplication.class, args);
    }

}
