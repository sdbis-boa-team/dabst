package com.sdbis.dabst.exception;

import java.util.Date;

public class ExceptionDetails {
    private final Date timestamp;
    private final String message;
    private final StackTraceElement[] trace;
    private final String details;

    public ExceptionDetails(Date timestamp, String message, StackTraceElement[] trace, String details) {
        super();
        this.timestamp = timestamp;
        this.message = message;
        this.trace = trace;
        this.details = details;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public String getMessage() {
        return message;
    }

    public StackTraceElement[] getTrace() {
        return trace;
    }

    public String getDetails() {
        return details;
    }
}
