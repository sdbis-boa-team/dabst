package com.sdbis.dabst.repository;

import com.sdbis.dabst.model.Database;

import java.util.List;

public interface CustomDatabaseRepository {
    List<Database> findOrderedByViewCountLimitedTo(Integer limit);
}
