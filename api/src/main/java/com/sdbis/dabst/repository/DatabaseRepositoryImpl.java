package com.sdbis.dabst.repository;

import com.sdbis.dabst.model.*;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Repository
public class DatabaseRepositoryImpl implements CustomDatabaseRepository {

    @PersistenceContext
    private EntityManager em;

    @Override
    public List<Database> findOrderedByViewCountLimitedTo(Integer limit) {
        return em.createQuery("SELECT d FROM Database d ORDER BY d.viewCount DESC",
                Database.class).setMaxResults(limit).getResultList();
    }

    public List<Database> searchByCriteria(
            @Nullable String name,
            @Nullable String langWrittenIn,
            @Nullable String langSupported,
            @Nullable String license,
            @Nullable String os,
            @Nullable String type
    ) {
        if (
                null == name &&
                null == langWrittenIn &&
                null == langSupported &&
                null == license &&
                null == os &&
                null == type
        ) {
            return new ArrayList<>();
        }

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Database> cq = cb.createQuery(Database.class);

        Root<Database> databaseRoot = cq.from(Database.class);

        if (null != name) {
            Predicate namePredicate = cb.like(cb.lower(databaseRoot.get("name")), "%" + name.toLowerCase() + "%");
            cq.where(namePredicate);
        }

        TypedQuery<Database> query = em.createQuery(cq);

        List<Database> databases = query.getResultList();

        if (null != langWrittenIn) {
            databases = filterByLanguageWrittenIn(databases, langWrittenIn);
        }

        if (null != langSupported) {
            databases = filterByLanguageSupported(databases, langSupported);
        }

        if (null != license) {
            databases = filterByLicense(databases, license);
        }

        if (null != os) {
            databases = filterByOS(databases, os);
        }

        if (null != type) {
            databases = filterByType(databases, type);
        }

        return databases;
    }

    private List<Database> filterByLanguageWrittenIn(List<Database> databases, String langWrittenIn) {
        List<Database> result = new ArrayList<>(databases);

        for (Database database : databases) {
            List<ProgrammingLanguage> languages = database.getProgrammingLanguagesWrittenIn();

            if (languages.size() == 0) {
                result.remove(database);
                continue;
            }

            boolean found = false;
            for (ProgrammingLanguage language : languages) {
                if (Objects.equals(language.getSlug().toLowerCase(), langWrittenIn.toLowerCase())) {
                    found = true;
                    break;
                }
            }

            if (!found) {
                result.remove(database);
            }
        }

        return result;
    }

    private List<Database> filterByLanguageSupported(List<Database> databases, String langSupported) {
        List<Database> result = new ArrayList<>(databases);

        for (Database database : databases) {
            List<ProgrammingLanguage> languages = database.getProgrammingLanguagesSupported();

            if (languages.size() == 0) {
                result.remove(database);
                continue;
            }

            boolean found = false;
            for (ProgrammingLanguage language : languages) {
                if (Objects.equals(language.getSlug().toLowerCase(), langSupported.toLowerCase())) {
                    found = true;
                    break;
                }
            }

            if (!found) {
                result.remove(database);
            }
        }

        return result;
    }

    private List<Database> filterByLicense(List<Database> databases, String license) {
        List<Database> result = new ArrayList<>(databases);

        for (Database database : databases) {
            List<License> licenses = database.getLicenses();

            if (licenses.size() == 0) {
                result.remove(database);
                continue;
            }

            boolean found = false;
            for (License lic : licenses) {
                if (Objects.equals(lic.getSlug().toLowerCase(), license.toLowerCase())) {
                    found = true;
                    break;
                }
            }

            if (!found) {
                result.remove(database);
            }
        }

        return result;
    }

    private List<Database> filterByOS(List<Database> databases, String os) {
        List<Database> result = new ArrayList<>(databases);

        for (Database database : databases) {
            List<OperatingSystem> operatingSystems = database.getOperatingSystems();

            if (operatingSystems.size() == 0) {
                result.remove(database);
                continue;
            }

            boolean found = false;
            for (OperatingSystem operatingSystem : operatingSystems) {
                if (Objects.equals(operatingSystem.getSlug().toLowerCase(), os.toLowerCase())) {
                    found = true;
                    break;
                }
            }

            if (!found) {
                result.remove(database);
            }
        }

        return result;
    }

    private List<Database> filterByType(List<Database> databases, String type) {
        List<Database> result = new ArrayList<>(databases);

        for (Database database : databases) {
            List<ProjectType> projectTypes = database.getProjectTypes();

            if (projectTypes.size() == 0) {
                result.remove(database);
                continue;
            }

            boolean found = false;
            for (ProjectType projectType : projectTypes) {
                if (Objects.equals(projectType.getSlug().toLowerCase(), type.toLowerCase())) {
                    found = true;
                    break;
                }
            }

            if (!found) {
                result.remove(database);
            }
        }

        return result;
    }
}
