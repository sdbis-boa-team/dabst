package com.sdbis.dabst.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "database")
public class Database implements Serializable {
    @Id
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "slug", nullable = false)
    private String slug;

    @Column(name = "url", nullable = false)
    private String url;

    @Column(name = "description", nullable = false)
    private String description;

    @Column(name = "start_year", nullable = false)
    private Integer startYear;

    @Column(name = "end_year", nullable = false)
    private Integer endYear;

    @Column(name = "acquired_by", nullable = false)
    private String acquiredBy;

    @Column(name = "developer", nullable = false)
    private String developer;

    @Column(name = "logo", nullable = false)
    private String logo;

    @Column(name = "former_names", nullable = false)
    private String techDocs;

    @Column(name = "source_url", nullable = false)
    private String sourceUrl;

    @Column(name = "wikipedia_url", nullable = false)
    private String wikipediaUrl;

    @Column(name = "view_count", nullable = false)
    private Integer viewCount;

    @ManyToMany
    @JoinTable(
            name="database_project_types",
            joinColumns=@JoinColumn(name="database_id", referencedColumnName="id"),
            inverseJoinColumns=@JoinColumn(name="project_type_id", referencedColumnName="id"))
    private List<ProjectType> projectTypes;

    @ManyToMany
    @JoinTable(
            name="database_written_in",
            joinColumns=@JoinColumn(name="database_id", referencedColumnName="id"),
            inverseJoinColumns=@JoinColumn(name="programming_language_id", referencedColumnName="id"))
    private List<ProgrammingLanguage> programmingLanguagesWrittenIn;

    @ManyToMany
    @JoinTable(
            name="database_supported_languages",
            joinColumns=@JoinColumn(name="database_id", referencedColumnName="id"),
            inverseJoinColumns=@JoinColumn(name="programming_language_id", referencedColumnName="id"))
    private List<ProgrammingLanguage> programmingLanguagesSupported;

    @ManyToMany
    @JoinTable(
            name="database_licenses",
            joinColumns=@JoinColumn(name="database_id", referencedColumnName="id"),
            inverseJoinColumns=@JoinColumn(name="license_id", referencedColumnName="id"))
    private List<License> licenses;

    @ManyToMany
    @JoinTable(
            name="database_oses",
            joinColumns=@JoinColumn(name="database_id", referencedColumnName="id"),
            inverseJoinColumns=@JoinColumn(name="os_id", referencedColumnName="id"))
    private List<OperatingSystem> operatingSystems;

    @ManyToMany
    @JoinTable(
            name="database_compatible_with",
            joinColumns=@JoinColumn(name="database_id", referencedColumnName="id"),
            inverseJoinColumns=@JoinColumn(name="database_compatible_with_id", referencedColumnName="id"))
    private List<Database> databasesCompatibleWith;

    @ManyToMany
    @JoinTable(
            name="database_derived_from",
            joinColumns=@JoinColumn(name="database_id", referencedColumnName="id"),
            inverseJoinColumns=@JoinColumn(name="database_derived_from_id", referencedColumnName="id"))
    private List<Database> databasesDerivedFrom;

    @ManyToMany
    @JoinTable(
            name="database_inspired_by",
            joinColumns=@JoinColumn(name="database_id", referencedColumnName="id"),
            inverseJoinColumns=@JoinColumn(name="database_inspired_by_id", referencedColumnName="id"))
    private List<Database> databasesInspiredBy;

    @ManyToMany
    @JoinTable(
            name="database_embedded",
            joinColumns=@JoinColumn(name="database_id", referencedColumnName="id"),
            inverseJoinColumns=@JoinColumn(name="database_embedded_id", referencedColumnName="id"))
    private List<Database> databasesEmbedded;

    public Database() {
    }

    public Database(Long id, String name, String slug, String url, String description, Integer startYear, Integer endYear, String acquiredBy, String developer, String logo, String techDocs, String sourceUrl, String wikipediaUrl) {
        this.id = id;
        this.name = name;
        this.slug = slug;
        this.url = url;
        this.description = description;
        this.startYear = startYear;
        this.endYear = endYear;
        this.acquiredBy = acquiredBy;
        this.developer = developer;
        this.logo = logo;
        this.techDocs = techDocs;
        this.sourceUrl = sourceUrl;
        this.wikipediaUrl = wikipediaUrl;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getSlug() {
        return slug;
    }

    public String getUrl() {
        return url;
    }

    public String getDescription() {
        return description;
    }

    public Integer getStartYear() {
        return startYear;
    }

    public Integer getEndYear() {
        return endYear;
    }

    public String getAcquiredBy() {
        return acquiredBy;
    }

    public String getDeveloper() {
        return developer;
    }

    public String getLogo() {
        return logo;
    }

    public String getTechDocs() {
        return techDocs;
    }

    public String getSourceUrl() {
        return sourceUrl;
    }

    public String getWikipediaUrl() {
        return wikipediaUrl;
    }

    public Integer getViewCount() {
        return viewCount;
    }

    public List<ProjectType> getProjectTypes() {
        return projectTypes;
    }

    public List<ProgrammingLanguage> getProgrammingLanguagesWrittenIn() {
        return programmingLanguagesWrittenIn;
    }

    public List<ProgrammingLanguage> getProgrammingLanguagesSupported() {
        return programmingLanguagesSupported;
    }

    public List<License> getLicenses() {
        return licenses;
    }

    public List<OperatingSystem> getOperatingSystems() {
        return operatingSystems;
    }

    public List<String> getDatabasesCompatibleWith() {
        return getDatabasesNamesFromList(databasesCompatibleWith);
    }

    public List<String> getDatabasesDerivedFrom() {
        return getDatabasesNamesFromList(databasesDerivedFrom);
    }

    public List<String> getDatabasesInspiredBy() {
        return getDatabasesNamesFromList(databasesInspiredBy);
    }

    public List<String> getDatabasesEmbedded() {
        return getDatabasesNamesFromList(databasesEmbedded);
    }

    private List<String> getDatabasesNamesFromList(List<Database> databaseList)
    {
        List<String> names = new ArrayList<>();
        for (Database database: databaseList) {
            names.add(database.getName());
        }

        return names;
    }
}
