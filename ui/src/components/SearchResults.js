import React from "react";
import DatabaseDetails from "./DatabaseDetails";

class SearchResults extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            detailsForDbId: undefined,
        }
    }

    static getObjectsNamesAsString(arrayObject) {
        if (arrayObject === undefined || arrayObject.length === 0) {
            return 'Unknown';
        }

        let names = [];
        arrayObject.forEach(
            (object) => {
                names.push(object.name);
            }
        );

        return names.join(', ');
    }

    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="row mt-2">
                        <div className="col-2"><h3>Search results</h3></div>
                        <div className="col-2"/>
                        <div className="col-2"/>
                        <div className="col-2"/>
                        <div className="col-2"/>
                        <div className="col-2">
                            <button type="button"
                                    className="btn btn-outline-primary">Compare
                            </button>
                        </div>
                    </div>
                    <table className="table table-hover">
                        <thead>
                        <tr>
                            <th scope="col">Compare</th>
                            <th scope="col">Name</th>
                            <th scope="col">Written in</th>
                            <th scope="col">Supports</th>
                            <th scope="col">Licenses</th>
                            <th scope="col">Operating system</th>
                            <th scope="col">Project type</th>
                        </tr>
                        </thead>
                        <tbody>
                        {
                            this.props.results !== undefined &&
                            this.props.results.map(
                                (database) => {
                                    return (
                                        <tr key={database.id}>
                                            <th>
                                                <div className="form-check">
                                                    <input className="form-check-input" type="checkbox" value=""
                                                           id="compare"/>
                                                </div>
                                            </th>
                                            <td>
                                                <span className="fw-bold"
                                                      data-bs-toggle="modal"
                                                      data-bs-target="#dbDetailsModal"
                                                      onClick={() => {
                                                          this.setState({detailsForDbId: database.id});
                                                      }}
                                                >
                                                    {database.name}
                                                </span>
                                            </td>
                                            <td>{SearchResults.getObjectsNamesAsString(database.programmingLanguagesWrittenIn)}</td>
                                            <td>{SearchResults.getObjectsNamesAsString(database.programmingLanguagesSupported)}</td>
                                            <td>{SearchResults.getObjectsNamesAsString(database.licenses)}</td>
                                            <td>{SearchResults.getObjectsNamesAsString(database.operatingSystems)}</td>
                                            <td>{SearchResults.getObjectsNamesAsString(database.projectTypes)}</td>
                                        </tr>
                                    )
                                }
                            )
                        }
                        {
                            this.props.results.length === 0 && <tr className="text-center">
                                <td colSpan="7">No results, try something else.</td>
                            </tr>
                        }
                        </tbody>
                    </table>
                    <DatabaseDetails id={this.state.detailsForDbId}/>
                </div>
            </div>
        );
    }
}

export default SearchResults;
