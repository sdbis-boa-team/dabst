import React from "react";

class MostPopular extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            mostPopularDatabases: undefined,
        }
    }

    componentDidMount() {
        fetch('/backend/api/v1/databases/most-popular?limit=4')
            .then(response => response.json())
            .then(mostPopularDatabases => this.setState({mostPopularDatabases}));
    }

    processDbDescription(description) {
        if (description.length > 100) {
            return description.substring(0, 100) + " [...]";
        }

        return description;
    }

    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="col-1"/>
                    <div className="col-10">
                        <div className="row mt-3">
                            <h3>Most popular databases</h3>
                            <div className="card-group mt-1">
                                {
                                    this.state.mostPopularDatabases !== undefined &&
                                    this.state.mostPopularDatabases.map(
                                        (database) => {
                                            return (
                                                <div className="col-3 px-3" key={database.id}>
                                                    <div className="card">
                                                        <div style={{height: 75}}>
                                                            <img
                                                                src={"https://dbdb.io/media/" + database.logo + ".280x250_q85.png"}
                                                                className="card-img-top" alt="logo"/>
                                                        </div>
                                                        <div className="card-body">
                                                            <h5 className="card-title">{database.name}</h5>
                                                            <h6 className="card-subtitle mb-2 text-muted">
                                                                {"Developed by " + database.developer + " since " + database.startYear}
                                                            </h6>
                                                            <p className="card-text">{this.processDbDescription(database.description)}</p>
                                                            <p className="card-text"><small
                                                                className="text-muted">
                                                                {"Viewed " + database.viewCount + " times"}
                                                            </small>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            )
                                        }
                                    )
                                }
                            </div>
                        </div>
                    </div>
                    <div className="col-1"/>
                </div>
            </div>
        );
    }
}

export default MostPopular;
