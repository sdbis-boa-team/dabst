import React from "react";
import SearchResults from "./SearchResults";

class DatabaseDetails extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            database: {},
        }
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevProps.id !== this.props.id) {
            fetch('/backend/api/v1/databases/' + this.props.id)
                .then(response => response.json())
                .then(database => this.setState({database}));
        }
    }

    render() {
        const db = this.state.database;
        return (
            <div>
                <div className="modal fade" id="dbDetailsModal" tabIndex="-1"
                     aria-labelledby="dbDetailsModalLabel" aria-hidden="true">
                    <div className="modal-dialog modal-lg">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title" id="exampleModalLabel">Details
                                    about {db.name ?? ''}</h5>
                                <button type="button" className="btn-close"
                                        data-bs-dismiss="modal"
                                        aria-label="Close"/>
                            </div>
                            <div className="modal-body">
                                <div className="row">
                                    <div className="col-5">
                                        {
                                            db.logo &&
                                            <img src={"https://dbdb.io/media/" + db.logo + ".280x250_q85.png"} alt=""/>
                                        }
                                        <table className="table">
                                            <tbody>
                                            <tr>
                                                <td>Developer</td>
                                                <td>{db.developer ?? 'Not available'}</td>
                                            </tr>
                                            <tr>
                                                <td>Acquired by</td>
                                                <td>{db.acquiredBy !== '' ? db.acquiredBy : 'N/A'}</td>
                                            </tr>
                                            <tr>
                                                <td>Start year</td>
                                                <td>{db.startYear !== 0 ? db.startYear : 'N/A'}</td>
                                            </tr>
                                            <tr>
                                                <td>End year</td>
                                                <td>{db.endYear !== 0 ? db.endYear : 'N/A'}</td>
                                            </tr>
                                            <tr>
                                                <td>Written in</td>
                                                <td>{SearchResults.getObjectsNamesAsString(db.programmingLanguagesWrittenIn ?? [])}</td>
                                            </tr>
                                            <tr>
                                                <td>Supports</td>
                                                <td>{SearchResults.getObjectsNamesAsString(db.programmingLanguagesSupported ?? [])}</td>
                                            </tr>
                                            <tr>
                                                <td>Licenses</td>
                                                <td>{SearchResults.getObjectsNamesAsString(db.licenses ?? [])}</td>
                                            </tr>
                                            <tr>
                                                <td>Operating system</td>
                                                <td>{SearchResults.getObjectsNamesAsString(db.operatingSystems ?? [])}</td>
                                            </tr>
                                            <tr>
                                                <td>Project type</td>
                                                <td>{SearchResults.getObjectsNamesAsString(db.projectTypes ?? [])}</td>
                                            </tr>
                                            <tr>
                                                <td>Views</td>
                                                <td>{db.viewCount}</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        {
                                            db.url && <p><a href={db.url}>Homepage</a></p>
                                        }
                                        {
                                            db.wikipediaUrl && <p><a href={db.wikipediaUrl}>Wikipedia</a></p>
                                        }
                                        {
                                            db.sourceUrl && <p><a href={db.sourceUrl}>Source code</a></p>
                                        }
                                    </div>
                                    <div className="col-7">
                                        <p className="my-auto">{db.description !== '' ? db.description : 'Description is not available.'}</p>
                                    </div>
                                </div>
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-secondary"
                                        data-bs-dismiss="modal">Close
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default DatabaseDetails;
