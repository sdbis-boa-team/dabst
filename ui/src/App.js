import React from "react";
import MostPopular from "./components/MostPopular";
import SearchResults from "./components/SearchResults";

class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            programmingLanguages: undefined,
            licenses: undefined,
            operatingSystems: undefined,
            projectTypes: undefined,
            filters: {
                name: '',
                langWrittenIn: '',
                langSupported: '',
                license: '',
                os: '',
                type: '',
            },
            searchResults: undefined,
        }
    }

    componentDidMount() {
        ['programming-languages', 'licenses', 'operating-systems', 'project-types'].forEach(
            (resource) => {
                switch (resource) {
                    case 'programming-languages':
                        fetch('/backend/api/v1/' + resource + '/all')
                            .then(response => response.json())
                            .then(programmingLanguages => this.setState({programmingLanguages}));
                        break;
                    case 'licenses':
                        fetch('/backend/api/v1/' + resource + '/all')
                            .then(response => response.json())
                            .then(licenses => this.setState({licenses}));
                        break;
                    case 'operating-systems':
                        fetch('/backend/api/v1/' + resource + '/all')
                            .then(response => response.json())
                            .then(operatingSystems => this.setState({operatingSystems}));
                        break;
                    case 'project-types':
                        fetch('/backend/api/v1/' + resource + '/all')
                            .then(response => response.json())
                            .then(projectTypes => this.setState({projectTypes}));
                        break;
                    default:
                        break
                }
            }
        );
    }

    search = () => {
        let filters = new FormData();
        for (const filtersKey in this.state.filters) {
            let filter = this.state.filters[filtersKey];
            if (filter !== '') {
                filters.append(filtersKey, filter);
            }
        }

        let queryString = new URLSearchParams(filters).toString();

        if (queryString === '') {
            return;
        }

        this.setIsLoading();
        fetch('/backend/api/v1/databases?' + queryString)
            .then(response => {
                if (response.status === 200) {
                    return response.json();
                }
                throw new Error(response);
            })
            .then(searchResults => {
                this.setState({searchResults});
                this.setIsLoading();
            })
            .catch((error) => {
                console.error('Error: ', error);
            });
    }

    setIsLoading = () => {
        this.setState(prevState => ({
            isLoading: !prevState.isLoading,
        }));
    }

    handleChange = (e) => {
        const id = e.target.id;
        let value = e.target.value;

        if (e.target.type === 'checkbox') {
            value = e.target.checked;
        }

        this.setState(prevState => ({
            filters: {
                ...prevState.filters,
                [id]: value,
            }
        }));
    }

    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="col-1"/>
                    <div className="col-10">
                        <h1 className="text-center mt-4">Database Smart Web Tool</h1>
                        <div className="row">
                            <div className="col-1"/>
                            <div className="col-10">
                                <div className="row">
                                    <div className="col-12">
                                        <div className="mt-3">
                                            <input type="text" className="form-control"
                                                   placeholder="Search..."
                                                   id="name"
                                                   value={this.state.filters.name}
                                                   onChange={this.handleChange}
                                            />
                                        </div>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-4">
                                        <select className="form-select mt-3"
                                                aria-label="Programming Language Written In"
                                                id="langWrittenIn"
                                                value={this.state.filters.langWrittenIn}
                                                onChange={this.handleChange}
                                        >
                                            <option defaultValue value="">Select lang written in</option>
                                            {
                                                this.state.programmingLanguages !== undefined &&
                                                this.state.programmingLanguages.map(
                                                    (lang) => {
                                                        return (
                                                            <option key={lang.id} value={lang.slug}>{lang.name}</option>
                                                        )
                                                    }
                                                )
                                            }
                                        </select>
                                        <select className="form-select mt-3"
                                                aria-label="Programming Language Supported"
                                                id="langSupported"
                                                value={this.state.filters.langSupported}
                                                onChange={this.handleChange}
                                        >
                                            <option defaultValue value="">Select lang supported</option>
                                            {
                                                this.state.programmingLanguages !== undefined &&
                                                this.state.programmingLanguages.map(
                                                    (lang) => {
                                                        return (
                                                            <option key={lang.id} value={lang.slug}>{lang.name}</option>
                                                        )
                                                    }
                                                )
                                            }
                                        </select>
                                        <select className="form-select mt-3"
                                                aria-label="Programming Language Written In"
                                                id="license"
                                                value={this.state.filters.license}
                                                onChange={this.handleChange}
                                        >
                                            <option defaultValue value="">Select license</option>
                                            {
                                                this.state.licenses !== undefined &&
                                                this.state.licenses.map(
                                                    (license) => {
                                                        return (
                                                            <option key={license.id}
                                                                    value={license.slug}>{license.name}</option>
                                                        )
                                                    }
                                                )
                                            }
                                        </select>
                                    </div>
                                    <div className="col-4">
                                        <select className="form-select mt-3"
                                                aria-label="Operating System"
                                                id="os"
                                                value={this.state.filters.os}
                                                onChange={this.handleChange}
                                        >
                                            <option defaultValue value="">Select operating system</option>
                                            {
                                                this.state.operatingSystems !== undefined &&
                                                this.state.operatingSystems.map(
                                                    (os) => {
                                                        return (
                                                            <option key={os.id} value={os.slug}>{os.name}</option>
                                                        )
                                                    }
                                                )
                                            }
                                        </select>
                                        <select className="form-select mt-3"
                                                aria-label="Project type"
                                                id="type"
                                                value={this.state.filters.type}
                                                onChange={this.handleChange}
                                        >
                                            <option defaultValue value="">Select project type</option>
                                            {
                                                this.state.projectTypes !== undefined &&
                                                this.state.projectTypes.map(
                                                    (type) => {
                                                        return (
                                                            <option key={type.id} value={type.slug}>{type.name}</option>
                                                        )
                                                    }
                                                )
                                            }
                                        </select>
                                    </div>
                                    <div className="col-4">
                                        <div className="row mt-5">
                                            <div className="col-3"/>
                                            <div className="col-3">
                                                <button type="button"
                                                        className="btn btn-outline-primary btn-lg mt-4"
                                                        onClick={this.search}
                                                        disabled={this.state.isLoading}
                                                >Search
                                                </button>
                                            </div>
                                            <div className="col-3"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-1"/>
                        </div>
                        <div className="d-flex justify-content-center align-items-center">
                            <div className={"spinner-border" + (this.state.isLoading ? '' : 'd-none')} role="status">
                                <span className="visually-hidden">Loading...</span>
                            </div>
                        </div>
                    </div>
                    <div className="col-1"/>
                </div>
                {
                    this.state.searchResults === undefined && this.state.isLoading === false && <MostPopular/>
                }
                {
                    this.state.searchResults !== undefined && <SearchResults results={this.state.searchResults}/>
                }
            </div>
        );
    }
}

export default App;
