#!/usr/bin/env sh
set -eu

envsubst '${DABST_BACK_HOST}' < /etc/nginx/conf.d/backend.conf.template > /etc/nginx/conf.d/backend.conf

exec "$@"
