<?php
// PHP > 8.0.13 required

declare(strict_types=1);

require_once('functions.php');

/**
TRUNCATE TABLE database_project_types CASCADE;
TRUNCATE TABLE database_licenses CASCADE;
TRUNCATE TABLE database_oses CASCADE;
TRUNCATE TABLE database_countries CASCADE;
TRUNCATE TABLE database_supported_languages CASCADE;
TRUNCATE TABLE database_written_in CASCADE;
TRUNCATE TABLE database_derived_from CASCADE;
TRUNCATE TABLE database_inspired_by CASCADE;
TRUNCATE TABLE database_embedded CASCADE;
TRUNCATE TABLE database_compatible_with CASCADE;
TRUNCATE TABLE database CASCADE;
TRUNCATE TABLE license CASCADE;
TRUNCATE TABLE operating_system CASCADE;
TRUNCATE TABLE programming_language CASCADE;
TRUNCATE TABLE project_type CASCADE;
 */

$conn_string = "host=localhost port=5432 dbname=dabst user=pgsql password=6XcPnP8dxQmtUwru";
$dbconn = pg_connect($conn_string) or die('Could not connect: ' . pg_last_error());;

//$limit = 10;

// Project types

$query = 'select pk, fields_slug, fields_name from dbdb where model = \'core.projecttype\'';
$result = queryDb($dbconn, $query);

$projectTypes = getArrayWithResults($result);

// Licenses

$query = 'select pk, fields_slug, fields_name, fields_url from dbdb where model = \'core.license\'';
$result = queryDb($dbconn, $query);

$licenses = getArrayWithResults($result);

// OSes

$query = 'select pk, fields_slug, fields_name from dbdb where model = \'core.operatingsystem\'';
$result = queryDb($dbconn, $query);

$oses = getArrayWithResults($result);

// Programming languages

$query = 'select pk, fields_slug, fields_name from dbdb where model = \'core.programminglanguage\'';
$result = queryDb($dbconn, $query);

$programmingLanguages = getArrayWithResults($result);

// Databases
$query = 'select pk, fields_view_count, fields_slug, fields_name from dbdb where model = \'core.system\'' . (!empty($limit) ? ' LIMIT ' . $limit : '');
$result = queryDb($dbconn, $query);

$databases = getArrayWithResults($result);

// Databases additional data and joint tables

$database_project_types = [];
$database_licenses = [];
$database_oses = [];
$database_countries = [];
$database_supported_languages = [];
$database_written_in = [];
$database_derived_from = [];
$database_inspired_by = [];
$database_embedded = [];
$database_compatible_with = [];

foreach ($databases as $pk => &$data) {
    $query = 'select fields_url, fields_description, fields_meta, fields_start_year, fields_end_year, fields_history, fields_acquired_by, fields_developer, fields_logo, fields_countries, fields_former_names, fields_tech_docs, fields_source_url, fields_wikipedia_url, fields_project_types from dbdb
where model = \'core.systemversion\' and fields_system = \'' . $pk . '.0\' and fields_is_current = \'True\'' . (!empty($limit) ? ' LIMIT ' . $limit : '');
    $result = pg_query($query) or die('Query failed: ' . pg_last_error());
    while ($row = pg_fetch_array($result, null, PGSQL_ASSOC)) {
        foreach ($row as $colName => $colValue) {
            $colName = extractColumnName($colName);
            $colValue = $colValue === null ? '' : $colValue;

            if ($colName === 'project_types') {
                if (isLiteralArrayString($colValue)) {
                    $colValue = str_replace(['[', ']', ' '], '', $colValue);
                    $colValue = explode(',', $colValue);
                }

                $database_project_types[$pk] = $colValue ?? null;
                continue;
            }

            if ($colName === 'countries') {
                if (isLiteralArrayString($colValue)) {
                    $colValue = str_replace(['[', ']', ' '], '', $colValue);
                }
                $colValue = explode(',', $colValue);

                $database_countries[$pk] = $colValue ?? null;
                continue;
            }

            $data[$colName] = $colValue ?? null;
        }
    }

    $query = 'select fields_derived_from, fields_inspired_by, fields_embedded, fields_compatible_with, fields_licenses, fields_oses, fields_supported_languages, fields_written_in from dbdb where model = \'core.systemversionmetadata\' and pk = \'' . (int)$data['meta'] . '\'' . (!empty($limit) ? ' LIMIT ' . $limit : '');
    $result = pg_query($query) or die('Query failed: ' . pg_last_error());
    while ($row = pg_fetch_array($result, null, PGSQL_ASSOC)) {
        foreach ($row as $colName => $colValue) {
            $colName = extractColumnName($colName);
            $colValue = $colValue === null ? '' : $colValue;

            if (in_array($colName, ['licenses', 'oses', 'supported_languages', 'written_in', 'derived_from', 'inspired_by', 'embedded', 'compatible_with'])) {
                if (isLiteralArrayString($colValue)) {
                    $colValue = str_replace(['[', ']', ' '], '', $colValue);
                    $colValue = explode(',', $colValue);
                }

                ${'database_' . $colName}[$pk] = $colValue ?? null;
                continue;
            }

            $data[$colName] = $colValue ?? null;
        }
    }
}

//print_r($projectTypes);
//print_r($licenses);
//print_r($oses);
//print_r($programmingLanguages);
//print_r($databases);
//print_r($database_project_types);
//print_r($database_countries);
//print_r($database_licenses);
//print_r($database_oses);
//print_r($database_supported_languages);
//print_r($database_written_in);

// Generate inserts into database table
$sql = '';
foreach ($databases as $database) {
    $sql .= 'INSERT INTO public.database(
	id, slug, name, url, description, start_year, end_year, acquired_by, developer, logo, former_names, tech_docs, source_url, wikipedia_url, view_count)
	VALUES (' .
        getDbSafeValueFromArrayByKey($database, 'pk', 'int') . ', ' .
        getDbSafeValueFromArrayByKey($database, 'slug') . ', ' .
        getDbSafeValueFromArrayByKey($database, 'name') . ', ' .
        getDbSafeValueFromArrayByKey($database, 'url') . ', ' .
        getDbSafeValueFromArrayByKey($database, 'description') . ', ' .
        getDbSafeValueFromArrayByKey($database, 'start_year', 'int') . ', ' .
        getDbSafeValueFromArrayByKey($database, 'end_year', 'int') . ', ' .
        getDbSafeValueFromArrayByKey($database, 'acquired_by') . ', ' .
        getDbSafeValueFromArrayByKey($database, 'developer') . ', ' .
        getDbSafeValueFromArrayByKey($database, 'logo') . ', ' .
        getDbSafeValueFromArrayByKey($database, 'former_names') . ', ' .
        getDbSafeValueFromArrayByKey($database, 'tech_docs') . ', ' .
        getDbSafeValueFromArrayByKey($database, 'source_url') . ', ' .
        getDbSafeValueFromArrayByKey($database, 'wikipedia_url') . ', ' .
        getDbSafeValueFromArrayByKey($database, 'view_count', 'int') . ') ON CONFLICT (id) DO NOTHING;';
}

// Generate inserts into project_type table
foreach ($projectTypes as $projectType) {
    $sql .= 'INSERT INTO public.project_type(
	id, slug, name)
	VALUES (' .
        getDbSafeValueFromArrayByKey($projectType, 'pk', 'int') . ', ' .
        getDbSafeValueFromArrayByKey($projectType, 'slug') . ', ' .
        getDbSafeValueFromArrayByKey($projectType, 'name') . ') ON CONFLICT (id) DO NOTHING;';
}

// Generate inserts into license table
foreach ($licenses as $license) {
    $sql .= 'INSERT INTO public.license(
	id, slug, name, url)
	VALUES (' .
        getDbSafeValueFromArrayByKey($license, 'pk', 'int') . ', ' .
        getDbSafeValueFromArrayByKey($license, 'slug') . ', ' .
        getDbSafeValueFromArrayByKey($license, 'name') . ', ' .
        getDbSafeValueFromArrayByKey($license, 'url') . ') ON CONFLICT (id) DO NOTHING;';
}

// Generate inserts into programming_language table
foreach ($programmingLanguages as $programmingLanguage) {
    $sql .= 'INSERT INTO public.programming_language(
	id, slug, name)
	VALUES (' .
        getDbSafeValueFromArrayByKey($programmingLanguage, 'pk', 'int') . ', ' .
        getDbSafeValueFromArrayByKey($programmingLanguage, 'slug') . ', ' .
        getDbSafeValueFromArrayByKey($programmingLanguage, 'name') . ') ON CONFLICT (id) DO NOTHING;';
}

// Generate inserts into operating_system table
foreach ($oses as $os) {
    $sql .= 'INSERT INTO public.operating_system(
	id, slug, name)
	VALUES (' .
        getDbSafeValueFromArrayByKey($os, 'pk', 'int') . ', ' .
        getDbSafeValueFromArrayByKey($os, 'slug') . ', ' .
        getDbSafeValueFromArrayByKey($os, 'name') . ') ON CONFLICT (id) DO NOTHING;';
}

$joinTablesWithFkName = [
    'database_project_types' => 'project_type_id',
    'database_licenses' => 'license_id',
    'database_oses' => 'os_id',
    'database_countries' => 'country_abbreviation',
    'database_supported_languages' => 'programming_language_id',
    'database_written_in' => 'programming_language_id',
    'database_derived_from' => 'database_derived_from_id',
    'database_inspired_by' => 'database_inspired_by_id',
    'database_embedded' => 'database_embedded_id',
    'database_compatible_with' => 'database_compatible_with_id',
];

foreach ($joinTablesWithFkName as $joinTableName => $fkName) {
    foreach (${$joinTableName} as $dbPk => $fkValues) {
        foreach ($fkValues as $fkValue) {
            if ($fkName === 'country_abbreviation') {
                $fkValue = '\'' . (!empty($fkValue) ? $fkValue : 'N\A') . '\'';
            } else {
                $fkValue = (int)$fkValue;
                if ($fkValue === 0) {
                    continue;
                }
            }
            $sql .= 'INSERT INTO public.' . $joinTableName . '(
	    database_id, ' . $fkName . ')
	    VALUES (' .
                (int)$dbPk . ', ' .
                $fkValue . ') ON CONFLICT (database_id, ' . $fkName . ') DO NOTHING;';
        }
    }
}

var_dump($sql);
$result = queryDb($dbconn, $sql);

pg_free_result($result);
pg_close($dbconn);
