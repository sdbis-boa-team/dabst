<?php

declare(strict_types=1);

function getArrayWithResults($result): array
{
    $array = [];
    while ($row = pg_fetch_array($result, null, PGSQL_ASSOC)) {
        foreach ($row as $colName => $colValue) {
            $colName = extractColumnName($colName);
            $array[$row['pk']][$colName] = $colValue ?? null;
        }
    }

    return $array;
}

function extractColumnName(string $columnName): string
{
    return str_contains($columnName, 'fields_') ? substr($columnName, 7) : $columnName;
}

function queryDb($dbconn, string $sql)
{
    $queryResult = pg_query($dbconn, $sql);

    if (!$queryResult) {
        die('Query failed: ' . pg_last_error());
    } else {
        echo 'Query ran successfully.' . PHP_EOL;
    }

    return $queryResult;
}

function getDbSafeValueFromArrayByKey(array $array, string $key, string $forcedType = null): int|string
{
    if (empty($value = $array[$key])) {
        $value = '';
    }

    if (!empty($forcedType)) {
        settype($value, $forcedType);
    }

    return is_integer($value) ? $value : '\'' . htmlspecialchars($value, ENT_QUOTES) . '\'';
}

function isLiteralArrayString(string $string): bool
{
    return str_contains($string, '[') && str_contains($string, ']');
}
